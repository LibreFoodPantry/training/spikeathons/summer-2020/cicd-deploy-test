# Purpose
The project is intended to test the deployment of a docker container using Gitlab's CI / CD.

# Project
When run corrrectly through Docker Compose, the project displays the phrase "Hello World" at http://localhost/.

# Build Stage
The build stage consists only of the "build-image" job. It builds the Docker image using the "docker-compose build" script command. This ensures that the dockerfile has been written correctly and that the image can be built without issue.

# Push Stage
The push stage consists only of the "push-image" job. It tags the Docker image with the commit SHA (unique to every commit) and the branch (master). This allows a user to see both the latest image and the image associated with a specific commit. It then pushes these tags to the project's [container registry](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/cicd-deploy-test/container_registry).

# Deploy Stage
Does not exist yet. Should deploy project to a Kubernetes cluster located on a server. The cluster uses [Kind](https://kind.sigs.k8s.io/) in order to run nodes in Docker containers rather than on separate machines.